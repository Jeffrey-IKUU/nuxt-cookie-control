export default {
  barTitle: "Cookies",
  barDescription:
    "We gebruiken onze eigen cookies en third-party cookies om onze site te tonen en om te leren hoe deze gebruikt wordt, met het oog om de services die we verlenen te verbeteren. Door verder te gaan op onze site gaan we ervanuit dat hiermee akkoord gegaan wordt.",
  acceptAll: "Accepteer alles",
  declineAll: "Verwijder alles",
  manageCookies: "Beheer cookies",
  unsaved: "Er zijn niet-opgeslagen instellingen",
  close: "Sluiten",
  save: "Opslaan",
  necessary: "Noodzakelijke cookies",
  optional: "Optionele cookies",
  functional: "Functionele cookies",
  blockedIframe:
    "Om dit te kunnen bekijken dienen functionele cookies geaccepteerd te worden",
  here: "hier",
};

